@extends('dashboard.index')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-tools float-left">
                    <form action="{{ route('roles.index')}}" method="GET">
                        @csrf
                        <div style="height: 60%" class="input-group input-group-outline mt-3">
                            <label class="form-label">Name</label>
                            <input  type="text" class="form-control name" name="name" value="{{ request('name') }}">
                        </div>
                        <button style="margin: 5px" type="submit" class="btn btn-primary">Search
                            <i class="fas fa-search"></i>
                          </button>
                    </form>
                  </div> 
                <div class=" me-3 my-3 text-end">
                    <form action="{{ route('roles.create')}}">
                        <button type="submit" class="btn bg-gradient-dark mb-0" href="javascript:;"><i
                            class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New
                        Categories</button>
                    </form>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        #</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Permission</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)                                       
                                <tr>
                                    <td>
                                        {{ $role->id}}
                                    </td>
                                    <td>
                                        {{ $role->display_name}}
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                        @if($role->permissions)
                                            @foreach($role->permissions as $permission)
                                                <span class="badge badge-sm bg-gradient-success">                                                 
                                                    {!! $permission->display_name . '<br>'!!}                                                
                                                </span>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <div class="mt-2 d-flex justify-content-center">
                                            <form action="{{ route('roles.show', ['id' => $role->id] )}}">
                                                <button type="submit" rel="tooltip" class="btn btn-info btn-link btn btn-info me-2"
                                                    href="" data-original-title=""
                                                    title="">
                                                    <i class="material-icons">info</i>
                                                    <div class="ripple-container"></div>
                                                </button>  
                                            </form>
                                            <form action="{{ route('roles.edit', ['id' => $role->id])}}">
                                                <button type="submit" rel="tooltip" class="btn btn-warning btn-link btn  me-2"
                                                href="" data-original-title=""
                                                title="">
                                                <i class="material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </button>  
                                            </form>
                                            <form action="{{ route('roles.destroy', ['id' => $role->id] )}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Bạn có chắc chắn muốn xoá ?')" type="submit" class="btn btn-danger btn-link"
                                                data-original-title="" title="">
                                                <i class="material-icons">close</i>
                                                <div class="ripple-container"></div>
                                            </button>
                                            </form>
                                        </div>
                                    </td>      
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
        {{ $roles->appends(request()->all())->links() }}
  
    <x-footers.auth></x-footers.auth>
</div>
@endsection