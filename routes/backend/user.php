<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

Route::prefix('/users')->controller(UserController::class)->name('users.')->group(function () {
    Route::get('/information', 'information')->name('information');
    Route::get('/', 'index')->name('index')->middleware('hasPermission:users.index');
    Route::get('/create', 'create')->name('create')->middleware('hasPermission:users.create');
    Route::post('/', 'store')->name('store')->middleware('hasPermission:users.store');
    Route::get('/{id}', 'show')->name('show')->middleware('hasPermission:users.show');
    Route::get('/{id}/edit', 'edit')->name('edit')->middleware('hasPermission:users.edit');
    Route::put('/{id}', 'update')->name('update')->middleware('hasPermission:users.update');
    Route::delete('/{id}', 'destroy')->name('destroy')->middleware('hasPermission:users.destroy');
});
