<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'id' => '1',
                'name' => 'all',
                'display_name' => 'all',
            ],
            [
                'id' => '2',
                'name' => 'products.index',
                'display_name' => 'products.index',
            ],
            [
                'id' => '3',
                'name' => 'products.create',
                'display_name' => 'products.create',
            ],
            [
                'id' => '4',
                'name' => 'products.store',
                'display_name' => 'products.store',
            ],
            [
                'id' => '5',
                'name' => 'products.show',
                'display_name' => 'products.show',
            ],
            [
                'id' => '6',
                'name' => 'products.edit',
                'display_name' => 'products.edit',
            ],
            [
                'id' => '7',
                'name' => 'products.update',
                'display_name' => 'products.update',
            ],
            [
                'id' => '8',
                'name' => 'products.destroy',
                'display_name' => 'products.destroy',
            ],
            [
                'id' => '9',
                'name' => 'categories.index',
                'display_name' => 'categories.index',
            ],
            [
                'id' => '10',
                'name' => 'categories.create',
                'display_name' => 'categories.create',
            ],
            [
                'id' => '11',
                'name' => 'categories.store',
                'display_name' => 'categories.store',
            ],
            [
                'id' => '12',
                'name' => 'categories.show',
                'display_name' => 'categories.show',
            ],
            [
                'id' => '13',
                'name' => 'categories.edit',
                'display_name' => 'categories.edit',
            ],
            [
                'id' => '14',
                'name' => 'categories.update',
                'display_name' => 'categories.update',
            ],
            [
                'id' => '15',
                'name' => 'categories.destroy',
                'display_name' => 'categories.destroy',
            ],
            [
                'id' => '16',
                'name' => 'roles.index',
                'display_name' => 'roles.index',
            ],
            [
                'id' => '17',
                'name' => 'roles.create',
                'display_name' => 'roles.create',
            ],
            [
                'id' => '18',
                'name' => 'roles.store',
                'display_name' => 'roles.store',
            ],
            [
                'id' => '19',
                'name' => 'roles.show',
                'display_name' => 'roles.show',
            ],
            [
                'id' => '20',
                'name' => 'roles.edit',
                'display_name' => 'roles.edit',
            ],
            [
                'id' => '21',
                'name' => 'roles.update',
                'display_name' => 'roles.update',
            ],
            [
                'id' => '22',
                'name' => 'roles.destroy',
                'display_name' => 'roles.destroy',
            ],
            [
                'id' => '23',
                'name' => 'users.index',
                'display_name' => 'users.index',
            ],
            [
                'id' => '24',
                'name' => 'users.create',
                'display_name' => 'users.create',
            ],
            [
                'id' => '25',
                'name' => 'users.store',
                'display_name' => 'users.store',
            ],
            [
                'id' => '26',
                'name' => 'users.show',
                'display_name' => 'users.show',
            ],
            [
                'id' => '27',
                'name' => 'users.edit',
                'display_name' => 'users.edit',
            ],
            [
                'id' => '28',
                'name' => 'users.update',
                'display_name' => 'users.update',
            ],
            [
                'id' => '29',
                'name' => 'users.destroy',
                'display_name' => 'users.destroy',
            ],
        ];
        Permission::insert($data);
    }
}
