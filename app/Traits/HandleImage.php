<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    const QUALITY_IMG = 80;

    protected $width = 500;
    protected $height = 500;

    public function verify($image): bool
    {
        return !empty($image);
    }

    public function saveImage($image): string|null
    {
        if ($this->verify($image)) {
            $extension = $image->getClientOriginalExtension();
            $image = Image::make($image)->fit($this->width, $this->height)->encode($extension, self::QUALITY_IMG);
            $name = time() . '.' . $extension;
            $pathImage = $this->getPathSave($name);
            Storage::disk('image')->put($pathImage, $image);
            return $name;
        }
        return null;
    }

    public function updateImage($image, $currentImage): string|null
    {
        if ($this->verify($image)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($image);
        }
        return $currentImage;
    }

    public function deleteImage($name): void
    {
        if ($this->exitsImage($name) && !empty($name)) {
            Storage::disk('image')->delete($this->getPathSave($name));
        }
    }

    public function exitsImage($name): bool
    {
        return Storage::disk('image')->exists($this->getPathSave($name));
    }

    public function getPath($name): string
    {
        if ($this->verify($name)) {
            return Storage::disk('image')->url($this->getPathSave($name));
        }
        return asset('customs/images/' . config('constant.image_default'));
    }

    public function getPathSave($name): string
    {
        return $this->folder . '/' . $name;
    }
}
