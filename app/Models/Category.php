<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = ['name', 'parent_id'];
    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product');
    } 

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function scopeWithNameCategory($query, $name)
    {
        return $name ? $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%')) : $query;
    }
    
    public function scopeWithParent($query, $id)
    {
        return $id ? $query->where('parent_id', $id) : $query;
    }
}
