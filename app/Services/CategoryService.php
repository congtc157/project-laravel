<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function search($request)
    {
        $data = $request->all();
        return $this->categoryRepository->search($data);
    }

    public function paginate($page = 5)
    {
        return $this->categoryRepository->paginate($page);
    }

    public function create($request)
    {
        $data = $request->all();

        return $this->categoryRepository->create($data);
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $data = $request->all();
        return $this->categoryRepository->update($data, $id);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }
}
