<?php

namespace App\Services;

use App\Traits\HandleImage;
use App\Repositories\ProductRepository;

class ProductService
{
    use HandleImage;

    public $folder = 'products';

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function search($request = null)
    {
        $data = $request->all();
        return $this->productRepository->search($data);
    }

    public function paginate($page = 5)
    {
        return $this->productRepository->paginate($page);
    }

    public function create($request)
    {
        $data = $request->all();
        $data['image'] = $this->saveImage($request->file('image'));
        $product = $this->productRepository->create($data);
        $categories = $request->input('categories');

        if ($categories !== null && isset($categories[0])) {
            $product->syncCategories($categories);
        }

        return $product;
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $data = $request->all();

        $data['image'] = $this->updateImage($request->image, $product->image);
        $product = $this->productRepository->update($data, $id);
        $categories = $request->input('categories');

        if ($categories !== null && isset($categories[0])) {
            $product->syncCategories($categories);
        }
        return $product;
    }

    public function delete($id): bool
    {
        $product = $this->productRepository->findOrFail($id);
        $this->deleteImage($product->image);
        return $this->productRepository->delete($id);
    }
}
