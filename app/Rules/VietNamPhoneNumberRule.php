<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class VietNamPhoneNumberRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $phone_regex = '/^(0|\+84)(\d{9}|\d{10})$/';
        if (!preg_match($phone_regex, $value)) {
            $fail("The $attribute field must be a valid Vietnam phone number.");
        }
    }
}
