<?php

namespace App\Http\Resources;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray($request)
    {
        $request = [
            'products' => $this->collection,
        ];

        if ($this->resource instanceof LengthAwarePaginator) {
            $request['links'] = [
                'pathName' => $this->path(),
                'totalPage' => $this->lastPage(),
                'previousPageUrl' => $this->previousPageUrl(),
                'currentPage' => $this->currentPage(),
                'currentPageUrl' => $this->url($this->currentPage()),
                'nextPageUrl' => $this->nextPageUrl(),
            ];
        }
        return $request;
    }
}
