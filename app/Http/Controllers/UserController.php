<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\StoreRequest;
use App\Http\Requests\Users\UpdateRequest;
use Illuminate\Http\Request;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userService, $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }
    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        $roles = $this->roleService->all();
        return view('admins.users.index', compact('users', 'roles'));
    }

    public function create()
    {
        $roles = $this->userService->listRoles();
        return view('admins.users.create', compact('roles'));
    }

    public function store(StoreRequest $request)
    {
        $this->userService->create($request);
        return redirect()->route('users.index');
    }

    public function show($id)
    {
        $user = $this->userService->findOrFail($id);
        return view('admins.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->findOrFail($id);
        $roles = $this->userService->listRoles();
        return view('admins.users.edit', compact('user', 'roles'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $this->userService->delete($id);

        return redirect()->route('users.index');
    }
    public function information()
    {
        $id = Auth::user()->id;
        $user = $this->userService->findOrFail($id);
        return view('clients.users.information', compact('user'));
    }

}
