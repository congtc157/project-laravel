<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;

class UpdateProductTest extends TestCase
{

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteUpdate($id): string
    {
        return route('products.update', $id);
    }

    public function getRouteEdit($id): string
    {
        return route('products.edit', $id);
    }

    public function makeData(): array
    {
        return Product::factory()->make()->toArray();
    }

    public function createProduct()
    {
        return Product::factory()->create();
    }
    public function makeFile(): File
    {
        Storage::disk('image');
        return UploadedFile::fake()->image(time() . '.jpg');
    }

    /** @test */
    public function authenticated_user_can_update_users_if_data_pass_validate()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_OK);
        $product = Product::find($product->id);
        $this->assertEquals($dataProduct['name'], $product->name);
        $this->assertDatabaseHas('products', [
            'id' => $product['id'],
            'name' => $dataProduct['name'],
            'price' => $dataProduct['price'],
            'description' => $dataProduct['description'],
            'expiry' => $dataProduct['expiry'],
        ]);
    }
    /** @test */
    public function authenticated_user_can_not_update_product_if_name_is_null()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make([
            'name' => null
        ])->toArray();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.name', fn ($name) => $name->contains('Name không được để trống'))
                ->etc()
        );
    }
     /** @test */
     public function authenticated_user_can_not_update_product_if_price_is_not_invalid()
     {
         $this->loginUserAdmin();
         $product = $this->createProduct();
         $dataProduct = Product::factory()->make([
             'price' => 'haha123'
         ])->toArray();
         $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();
         $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
         $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
         $response->assertJson(
             fn (AssertableJson $json) => $json->has('errors')
                 ->where('errors.price', fn ($name) => $name->contains('Price phải là chữ số'))
                 ->etc()
         );
     }
     /** @test */
    public function authenticated_user_can_not_update_product_if_price_is_null()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make([
            'price' => null
        ])->toArray();
        $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.price', fn ($name) => $name->contains('Price không được để trống'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_description_is_not_invalid()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make([
            'description' => 123
        ])->toArray();
        $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.description', fn ($name) => $name->contains('Description không phải là số'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_description_is_null()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make([
            'description' => null
        ])->toArray();
        $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.description', fn ($name) => $name->contains('Description không được để trống'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_expiry_is_not_invalid()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make([
            'expiry' => 1 - 1 - 01
        ])->toArray();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.expiry', fn ($name) => $name->contains('Expiry sai định dạng'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_expiry_is_null()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make([
            'expiry' => null
        ])->toArray();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.expiry', fn ($name) => $name->contains('Expiry không được để trống'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_image_is_max_size()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make()->toArray();
        $file = UploadedFile::fake()->create('image.jpg', 10000);
        $dataProduct['image'] = $file;
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.image', fn ($name) => $name->contains('Quá dung lượng ảnh'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_image_is_not_correct()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make()->toArray();
        $file = UploadedFile::fake()->create('image.xls', 4000);
        $dataProduct['image'] = $file;
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.image', fn ($name) => $name->contains('Sai định dạng Image'))
                ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_update_product_if_product_not_exits()
    {
        $this->loginUserAdmin();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->put($this->getRouteUpdate(-1), $dataProduct);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->etc()
        );
    }
     /** @test */
     public function unauthenticated_user_can_not_update_product()
     {
         $product = $this->createProduct();
         $dataProduct = $this->makeData();
         $dataProduct['image'] = $this->makeFile();
         $response = $this->put($this->getRouteUpdate($product->id), $dataProduct);
 
         $response->assertStatus(Response::HTTP_FOUND);
         $response->assertRedirect(route('login'));
     }

     /** @test */
    public function authenticate_user_can_not_update_product_if_user_has_not_permission()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $product = $this->createProduct();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($product->id), $dataProduct);
        $response->assertForbidden();
    }

    /** @test */
    public function authenticated_user_can_get_products_edit()
    {
        $this->loginUserAdmin();
        $product = Product::factory()->create();
        $response = $this->getJson($this->getRouteEdit($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('status_code')
                ->has(
                    'data',
                    fn (AssertableJson $json) => $json
                        ->where('name', $product->name)
                        ->where('expiry', $product->expiry)
                        ->where('description', $product->description)
                        ->etc()
                )
                ->etc()
        );
        $response->assertJsonStructure(
            [
                'status_code',
                'data' => ['id', 'name', 'price', 'description', 'expiry', 'image', 'categories' => ['ids', 'names']],
            ]
        );
    }


}
