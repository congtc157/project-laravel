<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
   
    public function makeData()
    {
        return Product::factory()->create();
    }
    public function getRouteDestroy($id)
    {
        return route('products.destroy', $id);
    }
    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }
    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }

     /** @test */
     public function authenticated_user_can_delete_product_if_product_exist()
     {
         $this->loginUserAdmin();
         $product = Product::factory()->create();
         $countRecord = Product::count();
         $response = $this->delete($this->getRouteDestroy($product->id));
         $response->assertStatus(204);
         $this->assertDatabaseMissing('products', $product->toArray());
         $this->assertDatabaseCount('products', $countRecord - 1);
     }
      /** @test */
    public function authenticated_user_can_not_delete_product_if_product_not_exist()
    {
        $this->loginUserAdmin();
        $response = $this->deleteJson($this->getRouteDestroy(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->etc()
        );
    }
     /** @test */
     public function unauthenticated_user_can_not_delete_product()
     {
         $product = Product::factory()->create();
         $response = $this->delete($this->getRouteDestroy($product->id));
         $response->assertStatus(Response::HTTP_FOUND);
         $response->assertRedirect(route('login'));
     }
      /** @test */
    public function authenticate_user_can_not_delete_product_if_user_has_not_permission()
    {
        $this->loginUser();
        $product = Product::factory()->create();
        $countRecord = Product::count();
        $response = $this->deleteJson($this->getRouteDestroy($product->id));
        $response->assertForbidden();
        $this->assertDatabaseCount('products', $countRecord);
    }

}
