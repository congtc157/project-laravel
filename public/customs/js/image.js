"use strict";
export const image = (function () {
  const module = {};

  
  module.load = function (element) {
    const file = $(element)[0].files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const url = URL.createObjectURL(file);
      $(".image-preview").attr("src", url);
    };
  };

  module.remove = function () {
    $(".image-preview").attr("src", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png");
  };

  
  return module;
})(window.jQuery, document, window);
