import { product } from "./product.js";
import { main } from "../../js/main.js";
import { image } from "../../js/image.js";
import { IMAGE_DEFAULT_PRODUCT } from "./constant.js";

// Sự kiện thay đổi input ảnh
$(document).on("change", ".input-image", function() {
  image.load($(this));
});

// Sự kiện click nút lưu
$(document).on("click", ".btn-save", async function() {
  await product.save($(".product-data"));
  product.getList($('.search-form'));
});

// Sự kiện nhập liệu trường tên
$(document).on("keyup", ".name", main.debounce(function () {
  product.getList($('.search-form'));
}));

// Sự kiện thay đổi trường danh mục
$(document).on("change", ".category", main.debounce(function () {
  product.getList($('.search-form'));
}));

// Sự kiện click nút xóa
$(document).on("click", ".btn-delete", function (event) {
  event.preventDefault();
  product.delete($(this));
});

// Sự kiện click nút thao tác trên sản phẩm
$(document).on("click", ".product-action", function() {
  product.selectCategory();
  $(".product-data").attr("data-url", $(this).attr("data-url"));
  $(".product-data").attr("data-action-name", $(this).attr("data-action-name"));
  $(".image-input, .n-image").show();
  $(".hidden-image").hide();

  enableInputs();
  $(".btn-save").show();
});

// Sự kiện click nút chỉnh sửa
$(document).on("click", ".btn-edit", function() {
  product.fillDataForm($(this));
  $(".hidden-image, .image-input, .n-image").show();
  $(".product-data").attr("data-url", $(this).attr("data-url-update"));
  $(".product-data").attr("data-action-name", $(this).attr("data-action-name"));

  enableInputs();
  $(".btn-save").show();
});

// Sự kiện click nút hiển thị
$(document).on("click", ".btn-show", function() {
  product.fillDataForm($(this));
  $(".hidden-image").show();
  $(".image-input, .n-image").hide();
  $(".product-data").attr("data-url", $(this).attr("data-url-update"));
  $(".product-data").attr("data-action-name", $(this).attr("data-action-name"));

  disableInputs();
  $(".btn-save").hide();
});

// Sự kiện click phân trang
$(document).on("click", ".pagination-js a", function (event) {
  event.preventDefault();
  product.getList($(this));
});

// Sự kiện click nút tìm kiếm
$(document).on("click", ".btn-search", function (event) {
  event.preventDefault();
  product.getList($('.search-form'));
});

// Sự kiện khi modal được đóng
$("#exampleModal")[0].addEventListener('hidden.bs.modal', function (event) {
  $(".form-product-data")[0].reset();
  main.removeError();
  product.selectCategory();
  $(".name-product, .price-product, .expiry-product").attr('value', '');
  $(".o-image, .image-preview").attr('src', IMAGE_DEFAULT_PRODUCT);
  $(".description-product").val('');

  enableInputs();
  $(".btn-save").show();
});

// Hàm kích hoạt trường nhập liệu
function enableInputs() {
  $(".product-data textarea, .product-data select, .product-data input").prop("disabled", false);
}

// Hàm vô hiệu hóa trường nhập liệu
function disableInputs() {
  $(".product-data textarea, .product-data select, .product-data input").prop("disabled", true);
}

// Lấy danh sách sản phẩm khi trang được tải
product.getList($('.search-form'));
