const API_URL = 'http://localhost:8088'

export const LIST_CATEGORY = `${API_URL}/categories/getListCategory`;

export const IMAGE_DEFAULT_PRODUCT = `https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png`;